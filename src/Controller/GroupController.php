<?php

namespace App\Controller;

use App\Entity\GradeBook;
use App\Entity\ScholarGroup;
use App\Form\ScholarGroupType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GroupController extends AbstractController
{
    /**
     * @Route("/groups", name="groups")
     */
    public function groupsList(): Response
    {
        $groups = $this->getDoctrine()->getManager()->getRepository(ScholarGroup::class)->findAll();

        return $this->render('index.html.twig', [
            'groups' => $groups
        ]);
    }

    /**
     * @Route("/group/new", name="group_new")
     * @param Request $request
     * @return Response
     */
    public function newGroup(Request $request): Response
    {
        $scholarGroup = new ScholarGroup();
        $form = $this->createForm(ScholarGroupType::class, $scholarGroup);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ScholarGroup $scholarGroup */
            $scholarGroup = $form->getData();

            $scholarGroup->setGradeBook(new GradeBook());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($scholarGroup);
            $entityManager->flush();

            return $this->redirectToRoute('groups');
        }

        return $this->render('form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/group/remove/{id}", name="group_remove")
     * @param $id
     * @return RedirectResponse
     */
    public function removeGroup($id): RedirectResponse
    {
        $group = $this->getDoctrine()->getRepository(ScholarGroup::class)->findOneBy(['id' => $id]);

        if ($group) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($group);
            $entityManager->flush();
        }

        return $this->redirectToRoute('groups');
    }
}
