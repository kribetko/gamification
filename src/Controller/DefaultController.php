<?php

namespace App\Controller;

use App\Entity\ScholarGroup;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $groups = $this->getDoctrine()->getManager()->getRepository(ScholarGroup::class)->findAll();

        return $this->render('index.html.twig', [
            'groups' => $groups
        ]);
    }
}
