<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TableController extends AbstractController
{
    /**
     * @Route("/grade-book/{id}", name="grade-book")
     * @param $id
     * @return Response
     */
    public function gradeBookPage($id): Response
    {
        $gradeBook = $this->getDoctrine()->getRepository('App:GradeBook')->findOneBy(['id' => $id]);

        if ($gradeBook) {
            return $this->render('grade-book.html.twig', [
                'gradeBook' => $gradeBook
            ]);
        }

        return $this->redirectToRoute('index');
    }
}