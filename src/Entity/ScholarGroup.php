<?php


namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScholarGroupRepository")
 * @ORM\Table(name="scholar_group")
 */
class ScholarGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Scholar", mappedBy="group")
     */
    protected $scholars;

    /**
     * @var GradeBook
     * @ORM\OneToOne(targetEntity="App\Entity\GradeBook", inversedBy="group", cascade={"persist", "remove"})
     */
    protected $gradeBook;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Collection
     */
    public function getScholars(): Collection
    {
        return $this->scholars;
    }

    /**
     * @param Scholar $scholar
     */
    public function addScholar(Scholar $scholar): void
    {
        $this->scholars->add($scholar);
    }

    /**
     * @param Scholar $scholar
     */
    public function removeScholar(Scholar $scholar): void
    {
        $this->scholars->remove($scholar);
    }

    /**
     * @return GradeBook
     */
    public function getGradeBook(): GradeBook
    {
        return $this->gradeBook;
    }

    /**
     * @param GradeBook $gradeBook
     */
    public function setGradeBook(GradeBook $gradeBook): void
    {
        $gradeBook->setGroup($this);
        $this->gradeBook = $gradeBook;
    }
}