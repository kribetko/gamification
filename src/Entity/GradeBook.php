<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GradeBookRepository")
 * @ORM\Table(name="grade_book")
 */
class GradeBook
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var ScholarGroup
     * @ORM\OneToOne(targetEntity="App\Entity\ScholarGroup", mappedBy="gradeBook")
     */
    protected $group;

    /**
     * @return ScholarGroup
     */
    public function getGroup(): ScholarGroup
    {
        return $this->group;
    }

    /**
     * @param ScholarGroup $group
     */
    public function setGroup(ScholarGroup $group): void
    {
        $this->group = $group;
    }
}