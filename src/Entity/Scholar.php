<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScholarRepository")
 * @ORM\Table(name="scholar")
 */
class Scholar
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var ScholarGroup
     * @ORM\ManyToOne(targetEntity="App\Entity\ScholarGroup", inversedBy="scholars")
     */
    protected $group;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return ScholarGroup
     */
    public function getGroup(): ScholarGroup
    {
        return $this->group;
    }

    /**
     * @param ScholarGroup $group
     */
    public function setGroup(ScholarGroup $group): void
    {
        $this->group = $group;
    }
}